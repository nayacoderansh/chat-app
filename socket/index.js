/* socket.io for messaging real time */



var cookie = require('cookie');
var ObjectID = require('mongodb').ObjectID;
var session = require('./../session')
var User = require('./../schema/user')
var Msg = require('./../schema/message')


// socket io conncetion 
module.exports = function(io){

	// authorize user if and only session cookie exists with the client
	// and the cookie matches our session in-memory
	io.set('authorization', function (handshakeData, accept) {
		if(cookie.parse(handshakeData.headers.cookie).session){
			for (var i = 0; i < session.length; i++) {
				if(session[i].session_id == cookie.parse(handshakeData.headers.cookie).session){
					// put session info of client in user_info
					handshakeData.user_info = session[i]
				} 
			}
			if(!handshakeData.user_info) {
				return accept('No cookie transmitted.', false);
			} else {
					  accept(null, true);
			}
		}
	});
	io.on('connection', function(socket) {
		// when a new user connects, pass socket.id inside our session in-memory
		// we need to store the socketId so that later we can use to push messages to specific client.
	  	var cookies =	cookie.parse(socket.request.headers.cookie)
		var user_session = cookies.session
		var newSession;
		for (var i = 0; i < session.length; i++) {
			if(session[i].session_id == user_session){
				newSession = session[i]
				newSession.socket_id = socket.id
				session.splice(i, 1)
				session.push(newSession)
				socket.request.user_info = newSession

			} 

		} 



		/** 
		Socket - 'message_counter' 
		Objective - Send new message status when a user connects.					
		*/ 
		Msg.findOne({$or:[ {'person_one':socket.request.user_info._id}, {'person_two':socket.request.user_info._id}]})
		.then(function(result){
			if(!result){}
				else {
					var inFar = 0
					result.conversation.forEach(function(list){
						console.log(socket.request.user_info._id, list.by, list.status )
						if((list.by.toString() != socket.request.user_info._id.toString()) && (!list.status)){
			
							inFar ++


						}
					})
					for (var i = 0; i < session.length; i++){
					if(session[i]._id == socket.request.user_info._id){
						io.sockets.in(session[i].socket_id).emit('message_counter', inFar);
						}
					}
	

				}
		})

		// --- ends 

		/** 
		Socket - 'new_message' 
		Objective - Recieve new messages sent by user, update the db.
					If the recipient is online, then push it to them.
		
		*/ 
		socket.on('new_message', function(message){
			var status;

			Msg.findOne({
				$and : [{	
					$or:[
						{'person_one': socket.request.user_info._id},
						{'person_two' : socket.request.user_info._id}
					]},{
					$or:[{'person_two': message.to},
						{'person_one' : message.to}
					]}	 

				]})
				
			

			.then(function(result){
				// if users have interacted before, we add it to their conversation. 
				// Else start a new conversation
				if(!result){
					status = true
					var MainObj = {}
					MainObj.person_one = socket.request.user_info._id
					MainObj.person_two = message.to
					MainObj.conversation = []
					MainObj.conversation.push({by : socket.request.user_info._id, msg : message.msg })
					return MainObj

				} else {

					result.conversation.push({by : socket.request.user_info._id, msg : message.msg})
					return result
				}


			})
			.then(function(result){
				var msg = new Msg(result);
				return msg.save()
			})
			.then(function(result){
				for (var i = 0; i < session.length; i++){
					if(session[i]._id == message.to){
						if(!status){
							// if a conversation between them exists, send a message
							io.sockets.in(session[i].socket_id).emit('new_message', {by : socket.request.user_info._id, msg : message.msg});

						} else {
							myObj = {
								_id : socket.request.user_info._id , 
								name : socket.request.user_info.name, 
								last_msg :  message.msg, 
								last_msg_timestamp : message.timestamp, 
								unread_count : 0
							}
							// if a conversation between them doesn't, update the people list

							io.sockets.in(session[i].socket_id).emit('person_list_add', myObj);


						}

					}
				}
			})
			.catch(function(err){
				console.log(err)
			})
		})

	/** 
		Socket - 'get_chat_list', 'recieved_list'
		Objective - Recieve user request for conversation data
					send the conversation data
		
		*/ 


    socket.on('get_chat_list',function(_id){

		Msg.findOne({
				$and : [{	
					$or:[
						{'person_one': socket.request.user_info._id},
						{'person_two' : socket.request.user_info._id}
					]},{
					$or:[{'person_two': _id},
						{'person_one' : _id}
					]}	 

				]})
		.populate('person_two person_one')
		.exec(function(err, result){
			var convo;
			if(err){console.log(err)}
			if(!result){console.log('no result')}	
			
			convo = result.conversation
			for (var i = 0; i < session.length; i++){
				if(socket.request.user_info._id.toString() == session[i]._id.toString()){
					
					io.sockets.in(session[i].socket_id).emit('recieved_list', {
						by : _id,
						conversation : convo

					});

				}
			}
			result.conversation.forEach(function(e){
				e.status = true
			})
			result.save()
				
        });
	})

})

}
'use strict';

var Mongoose 	= require('mongoose');
var bcrypt      = require('bcrypt-nodejs');
const SALT_WORK_FACTOR = 10;


// user schema 
// @user_type can take the following values : 'admin', 'patient', 'doctor'
var UserSchema = new Mongoose.Schema({
    name: { type: String, required: true},
    email: { type: String, required: true, unique : true},
    password: { type: String},
    specialization : String,
    bio : String,
    h_issue : String,
    user_type : String
});



// hash user password before saving

UserSchema.pre('save', function(next) {
    var user = this;


    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, null, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});



/**
 * Create an Instance method to validate user's password
 * This method will be used to compare the given password with the passwoed stored in the database
 * 
 */
UserSchema.methods.validatePassword = function(password, callback) {
    bcrypt.compare(password, this.password, function(err, isMatch) {
        if (err) return callback(err);
        callback(null, isMatch);
    });
};

// Create a user model
var userModel = Mongoose.model('user', UserSchema, 'user');

module.exports = userModel;
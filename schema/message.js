'use strict';

var Mongoose  = require('mongoose');

/**
* Message schema
* Our design choice here is to map two people 
* and subsequently add their msgs in conversation array.
* person_one represents the person who began the conversation by sending the first message
* person_two represents the reciepent of the first message 
*/

var messageSchema = new Mongoose.Schema({
	person_one : {required:true, ref:"user", type: Mongoose.Schema.Types.ObjectId},
	person_two : {required:true,ref:"user", type: Mongoose.Schema.Types.ObjectId},
	conversation : [{
		by : {ref:"user", type: Mongoose.Schema.Types.ObjectId},
		msg : String,
		status : {type : Boolean, default : false},
		timestamp : { type : Date, default : Date.now()}
	}],
	timestamp : { type : Date, default : Date.now()}
    
});

var Model = Mongoose.model('msg', messageSchema, 'msg');

module.exports = Model;
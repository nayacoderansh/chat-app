'use strict';

var app = {
  main: function(){
    var socket = io();
    var myBookId;
    var myBookName;


    socket.on('message_counter', function(counter){

      document.getElementById("message_counter").innerHTML = counter

    })

    socket.on('new_message', function(message){

       document.getElementById('message_counter').innerHTML =  parseInt(document.getElementById('message_counter').innerHTML)+1 

    })
  	
    // open dialog box  
    $(document).on("click", ".open-AddBookDialog", function () {
       	myBookId = $(this).data('id');  
        myBookName = $(this).data('name');
    });

    // send new message
    $("#saveMsg").on('click', function(e) {

      var textareaEle = $("textarea[name='message']");
      var messageContent = textareaEle.val().trim();
      if(messageContent !== '') {
          var message = { 
            msg: messageContent, 
            to: myBookId,
            name : myBookName,
            timestamp: Date.now()
          };

        socket.emit('new_message', message);
         document.getElementById("close_model").click();
        
      }
    });
},

message : function(){

   
  var socket = io();
  var convo_active;
   socket.on('disconnect', function(res){
      document.location.href="/";

    })
  // new person added in chat list
  socket.on('person_list_add', function(message){
    console.log(message)
      var html = `
      <a id='conv_open' href='#'   data-id=\'${message._id}\'  class="list-group-item list-group-item-action flex-column align-items-start active">
      <div class="d-flex w-100 justify-content-between">
      <h5 class="mb-1"> ${message.name}</h5>
      <small class="text-muted">just now</small>
      </div>
      <p class="mb-1">${message.last_msg}.</p>
      </a>`;
      var d1 = document.getElementById('person_list');
      d1.insertAdjacentHTML('beforebegin', html);

  })

  // new message recieved
  socket.on('new_message', function(message){
   // if new msg is recieved from chat which is currently open
    if(message.by == convo_active){

        var html = `
         <li class="col-md-6  mb-5 mt-5  list-group-item list-group-item-action d-flex active">

             <p class="mb-1">      ${message.msg}</p>
        </li>`
          
          var d1 = document.getElementById('chat_list_con');
          d1.insertAdjacentHTML('beforeend', html);
          var objDiv = document.getElementById("box");
          objDiv.scrollTop = objDiv.scrollHeight; 
    } else {
      // if chat of new msg sender is not active
      document.getElementById(message.by).innerHTML = parseInt(document.getElementById(message.by).innerHTML)+1 
      document.getElementById("text" + message.by).innerHTML = message.msg
    }
 })

// Get the input field
var input = document.getElementById("text_areas");

// Execute a function when the user releases a key on the keyboard
input.addEventListener("keyup", function(event) {
  // Cancel the default action, if needed
  event.preventDefault();
  // Number 13 is the "Enter" key on the keyboard
  if (event.keyCode === 13) {
    // Trigger the button element with a click
    document.getElementById("button-addon2").click();
  }
});

    // enter a new message in chat
   $("#button-addon2").on('click', function(e) {

      var textareaEle = $("textarea[name='new_message']");
      var messageContent = textareaEle.val().trim();
      if(messageContent !== '') {
          var message = { 
            msg: messageContent, 
            to : convo_active,
            timestamp: Date.now()
          };

        socket.emit('new_message', message);
        textareaEle.val('');

      var html = `
        <li class="col-md-6 offset-md-6  mb-5 mt-5  list-group-item list-group-item-action d-flex ">

             <p class="mb-1" style="overflow: hidden;">
             ${messageContent}
             </p>
        </li>  
      `;
      var d1 = document.getElementById('chat_list_con');
      d1.insertAdjacentHTML('beforeend', html);
        var objDiv = document.getElementById("box");
        objDiv.scrollTop = objDiv.scrollHeight; 
      }
    });


   // open conversation
   $(document).on("click", "#conv_open", function () {
    if(convo_active == $(this).data('id') ){} else {


      convo_active = $(this).data('id');
      socket.emit('get_chat_list', convo_active)
      document.getElementById('chat_list_con').innerHTML = ''
     } 
   })


   // get chat list 

   socket.on('recieved_list', function(list){
     var total_html = []
      if(list.by.toString() == convo_active.toString()){
        list.conversation.forEach(function(e){
        var html;
        if(e.by != convo_active ){
          html = `
              <li class="col-md-6 offset-md-6  mb-5 mt-5  list-group-item list-group-item-action d-flex ">
              <p class="mb-1" style="overflow: hidden;">
             ${e.msg}
             </p>
             </li>  `
        } else {
          html = `
          <li class="col-md-6  mb-5 mt-5  list-group-item list-group-item-action d-flex active">

             <p class="mb-1">      ${e.msg}</p>
        </li>
          `
        }
        total_html.push(html)
      })
      }
  

      total_html.join('')
      var d1 = document.getElementById('chat_list_con');
      d1.insertAdjacentHTML('beforeend', total_html);
        var objDiv = document.getElementById("box");
        objDiv.scrollTop = objDiv.scrollHeight; 
    
        // read all on open


        document.getElementById(list.by).innerHTML = 0



   })


}


}
**Chat App**
This application has the following features

1. Multiple User types
2. Use of In-Memory session
3. socket,io for real time chat
4. And the basic, login, creation, updation of users.


---

## Dependencies 

This application is built on NodeJS. 
Below list the list of dependencies and the problem they solve.

1. ExpressJS : Handle api requests and sort of the architectural foundation
2. Mongoose : MongoDB ODM
3. bcrypt-nodejs : Hashing and matching user password
4. body-parser : parsing body from client 
5. cookie-parser : parsing cookies from client
6. ejs : Templating engine, supports html
7. morgan : Logging
8. socket.io : Handling websockets between server and client
9. serve-favicon : Serve favicon 




var express = require('express');
var router = express.Router();

// Our Mongo Schema
var User = require('./../schema/user')
var Msg = require('./../schema/message')

// auth for handling authentication
var auth = require('./../auth')
var session = require('./../session')




/*
	
	Overview : This route handles user operations like creation and updation.
	Detail : This route handles : 
	 			1. User creation by admin
	 			2. User updation by admin
	 			3. User updation by user 
	In third case, a user can only update his own data. 			
	In which case, we're updating our session data with the modified valie

*/
router.post('/profile',auth, function(req, res, next){


	var query;
	var body = req.body;
	if(req.query.data){
		query = User.findByIdAndUpdate( req.query.data,body, { new: true })
	} else {
		var newUser = new User(req.body);
		query = newUser.save()
	}

	query
	.then(function(result){
		var id = req.query.data || result._id
		console.log(req.user._id, req.query.data )
		if(req.user._id == req.query.data){
			var newObj;
			for (var i = 0; i < session.length; i++) {
				if(session[i]._id == req.query.data){
					session.splice(i, 1)
					newObj = result
					newObj.session_id = req.cookies.session
					session.push(newObj)

				}
			} 

			

			res.redirect(`/?updated=true`)


		} else {
			res.redirect(`/profile/${id}?success=01`)
		}
	})
	.catch(function(err){
		
      if (err.name === 'MongoError' && err.code === 11000) {
			res.redirect('/profile/new?error=11000')
		} else {
			res.redirect('/profile/new?error=01')	
		}
	})
  
})


/*

	Overview : Handles login of users
	Detail : This route handles login requests by admin, doctor and patient
	We check their 
	1. email : Match with our db
	2. Password : Match it with hashed password in db
	3. user_type : type mentioned in param must match with document with email and type.
	
	When a user passes all the checks, We store their session in-memory and redirect to profile.
*/

router.post('/:type', function(req, res, next) {

	var type = req.params.type,
		body = req.body	,
	 	ram=Math.random().toString();
	ram=ram.substring(2,ram.length);
	

	User.findOne({email : body.email}, function(err, user){		
		if(err) throw err;
		if(!user){
			res.redirect('/?error=02');
		} else if (user.user_type != type){
			res.redirect('/?error=02');

		} else {
			user.validatePassword(body.password, function(err, isMatch) {
				if(err) throw err;
        		if (!isMatch){
        			res.redirect('/?error=01');
        		} else {
        			user.session_id = ram
        			session.push(user)
        			res.cookie('session',ram, { maxAge: 900000, httpOnly: true });
        			res.redirect('/')
        		}
	  				
      		});
		}
	})


});








module.exports = router;


var session = require('./../session')
var auth = require('./../auth')

var User = require('./../schema/user')
var Msg = require('./../schema/message')
var express = require('express');
var router = express.Router();



/*
	Overview : 	Homepage 
    Detail :This route handles visit to website homepage. 
			We check the session, and if session is valid we send them to their profile view
			If session doesn't exist or is not valid, we send them to login state 
*/

router.get('/',auth, function(req, res, next) {

	if(!req.user){
		var error = req.query.error || null
		res.render('index', {error : error});

	} else {


		res.render('profile', {user : req.user});
		
	}
	
});

/*
	Overview : 	Logout 
    Detail :We remove session from client and in-memory 
*/
router.get('/logout', function(req, res, next){
	
	for (var i = 0; i < session.length; i++) {
		if(session[i].session_id == req.cookies.session){
			session.splice(i,1)
		}
	}
	res.clearCookie("session");
  	res.redirect('/')

})


/*
	Overview : 	Messages state 
   Detail : Since we manipulate data from db before sending to client,
			majorly to provide ease on client side. We list the operations below
			1. Find all chats by user in db and populate person info
			2. Since we change format, we create new variables
			3. Send the modified data to message view
*/

router.get('/messages',auth, function(req, res, next){
	Msg.find({$or:[ {'person_one':req.user._id}, {'person_two':req.user._id}]}).populate('person_two person_one')
	.exec(function(err, result){
		if(err){console.log(err); res.redirect('/')}
		// we will store our new array in this variable
		var MsgArray = []

		result.forEach(function(convo){
			var MsgObj = {}

			/*
				Since a chat contains two party,
				its important for us find data only for person other than the user itself
			*/
			if(convo.person_one._id.toString() == req.user._id.toString()){
				
				MsgObj._id = convo.person_two._id
				MsgObj.name = convo.person_two.name
			} else {
				MsgObj._id = convo.person_one._id
				MsgObj.name = convo.person_one.name

			}

			MsgObj.last_msg = convo.conversation[convo.conversation.length - 1].msg
			MsgObj.last_msg_timestamp = convo.conversation[convo.conversation.length - 1].timestamp
			MsgObj.conversation = convo.conversation
			MsgObj.unread_count = 0 
			convo.conversation.forEach(function(msg){
				if((msg.by.toString() != req.user._id.toString()) && (msg.status == false)){
					MsgObj.unread_count ++
				}
			})
			MsgArray.push(MsgObj)
		})
		res.render('messages', {user : req.user, msg_list : MsgArray })

	})

})

/*
	Overview : 	User form state 
   	Detail : We handle both new creation and updation in this view.
   		 	1. When admin adds or modifies data
   		 	2. When user modifies his own data
   	The parameter id either holds id of the modifying user or is set to new
   	representing that a new user creation.
   	If user data is being modified we must show the data, in which case, we pull the data from db 
   	and push it to view 	 	
*/

router.get('/profile/:id',auth, function(req, res, next){
	var error = req.query.error || null
	var success = req.query.success || null


	if(req.user && ((req.user.user_type == 'admin') || (req.user._id == req.params.id))){

		if(req.params.id != 'new'){
			User.findById(req.params.id).then(function(result){
				res.render('people_form', {user : req.user,form_data:result, error : error, success : success})	
			})
		} else {
			res.render('people_form', {user : req.user, form_data: null, error : error, success : success})	
		}
	} else {
		res.redirect('/')
	}
	

})

/*
	Overview : 	A configuration route
   Detail : This route creates admin in db by default. 
*/
router.get('/default/val', function(req, res, next){
	var doc = {"_id":"5b50c1cd5da9bf7360cb0a66","name":"Dr One","email":"admin@something.com","bio":"I'm Admin ","password":"admin@123","user_type":"admin"}
	var newUser = new User(doc);
	newUser.save()
	.then(function(result){
		res.json(result)
	})
	.catch(function(err){
		res.json({success : false, data : err})
	})
})


/*
	Overview : 	List view
   Detail : We show list of doctors or patient through this view.
   The parameter list_type tells the user_type. We find the user in db 
   and push the list data on view. 
   
*/

router.get('/:list_type',auth, function(req, res, next) {
	
	if(!req.user){
		res.redirect('/')
	}
	var list_type = req.params.list_type

	User.find({user_type : list_type }).then(function(result){
		res.render('list',  {user : req.user, list : result, list_type : list_type })
	})



		

});


module.exports = router;
